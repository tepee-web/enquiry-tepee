$(document).ready(function() {
	$("a").click(function(event) {
	  event.preventDefault();
	  var anchor = $(this).attr('href');
		$("html,body").animate({scrollTop: $(anchor).offset().top}, 'slow');
	});
	$('.prettyCheckable').each(function(i) {
		$(this).prettyCheckable();
	});
	$("#contact-form input[type='submit']").click(function(e) {
    e.preventDefault();
    $('#contact-form .highlight').removeClass('highlight');
    $.ajax({
      url: "/ajax/contact.php",
      type: "post",
      data: $("#contact-form").serializeArray(),
	    success: function(response, textStatus, jqXHR){
	      var json = jQuery.parseJSON(response);
	      if (json.result == 'success') {
	        $('#contact-form .error').hide();
	        $('#contact-form .submit').hide();
					$('#contact-form .success .text').html(json.message);
		      $('#contact-form .success').fadeIn();
	      }
	      if (json.result == 'error') {
	        $('#contact-form .error .text').html(json.message);
		      $('#contact-form .error').show();
	        if (json.highlight.length > 0) {
		        json.highlight.forEach(function(id) {
						  $('#contact-form #'+id).parent().addClass('highlight');
						});
					  $('#contact-form #'+json.highlight[0]).focus();
	        }
	      }
	    },
	    error: function(jqXHR, textStatus, errorThrown){
	      alert(errorThrown);
			}
		});
	});
});
