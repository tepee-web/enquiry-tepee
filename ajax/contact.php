<?php
if (empty($_POST)) die ("No data was submitted");

extract($_POST);

function error($message, $highlight) {
  $result = 'error';
  if (!is_array($highlight)) $highlight = array($highlight);
	die(json_encode(compact('result', 'message', 'highlight')));
}

if (empty($name)) error('You must provide your name', 'name');
if (empty($email) && empty($phone)) error('You must provide an email address or phone number', array('email', 'phone'));
elseif (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) { error('The email address you have entered is not valid', 'email'); }
if (empty($message)) error('You must provide a message', 'message');

if (!empty($products)) {
	foreach ($products as $field => $value) {
		$product = str_replace("_", " ", $field);
		$product = ucwords($product);
		$productArr[] = $product;
	}
	$products = implode(", ", $productArr);
} else {
	$products = "No products selected";
}

if ($result == 'error') die(json_encode(compact('result', 'highlight', 'message')));

$body = "<html>
<body>
<h1>Warmflow Contact Form</h1>
<p><strong>Name:</strong> $name<br />
<strong>Email:</strong> $email<br />
<strong>Phone:</strong> $phone</p>
<p><strong>Message:</strong><br />$message</p>
<p><strong>Interested in:</strong> $products</p>";

mail("tim@tepeedesign.co.uk", "Warmflow Contact Form", $body, "Content-type: text/html");

$result = 'success';
$message = 'Thank you! Your message has been sent successfully.';

echo json_encode(compact('result', 'message'));
?>